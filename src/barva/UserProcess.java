package barva;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;



/*===========================================

 각 사용자 별로 처리할 작업. 거의 모든 시작이 이 쓰레드에서 시작한다.

 ============================================*/

public class UserProcess extends Thread {
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	String message;
	String cm = "CM";	//check message, 접속이 유지되고 있는지 체크
	String id;

	String[] messageList = {"SD","RD","AR","JOIN","LOGIN","CP","JP","AP","MM","GP","FH","EX"};


	public UserProcess(Socket socket) {
		this.socket = socket;
		DB.loadConnect();
		System.out.println("client : " +socket);
	}

	public void setID(String id){
		this.id = id;
	}


	public String getID() {
		return id;
	}
	@Override
	public void run() {
		try {


			//System.out.println("파일 수신 작업을 시작합니다.");
			dis = new DataInputStream(socket.getInputStream());
			dos = new DataOutputStream(socket.getOutputStream());

			while(true){	
				System.out.println("메세지 검사");
				message = dis.readUTF();
				System.out.println("메세지 : "+message);
				switch(message ){							//원하는 명령 수신
				case "CM":													//check message. 주기적으로 접속상태 체크, 업데이트사항 체크
					System.out.println("CM 호출");
					new Check(socket, dis, dos, this, messageList);
					break;
				case "SD":													//send data
					System.out.println("SD 호출");

					new FileHistory_Sender(socket, dos, dis);

					break;
				case "PR":													//Project Reload
					new projectReload(socket, dis, dos, this);
					break;


				case "RD":													//receive data
					System.out.println("RD 호출");
					new ReceiveData(socket, dis, dos);
					break;
				case "AR":
					System.out.println("AR 호출");                    	 //관련 파일 모두 전송
					new FileSender(socket, dis, dos, this);
					break;
				case "JOIN":												//회원가입
					System.out.println("JOIN 호출");
					new JoinProcess(socket, dis, dos);
					break;
				case "LOGIN":
					System.out.println("LOGIN 호출");							//로그인
					new LoginProcess(socket, dis, dos, this);
					break;

				case "CP":													
					System.out.println("CP 호출");							//create Project
					new CreateProject(socket, dis, dos, this);
					break;

				case "JP":													
					System.out.println("JP 호출");							//join Project
					new JoinProject(socket, dis, dos, this);
					break;

				case "AP":
					System.out.println("AP 호출");							//approve 승인목록 호출
					new getApprove(socket, dis, dos, this);
					break;

				case "MM":
					System.out.println("MM 호출");							//MemberModify 회원정보 수정
					new MemberModify(socket, dis, dos);
					break;

				case "GP":
					System.out.println("GP 호출");							//get Project 프로젝트 정보 조회 호출
					new getProject(socket, dis, dos, this);
					break;

				case "FH":
					System.out.println("FH 호출");                     //파일별 히스토리
					new FileHistory(socket, dis, dos);
					break;
					
				case "MC1":
					System.out.println("MC1 호출");                     //파일별 히스토리
					new ModifyComment1(socket, dis, dos);
					break;
					
				case "MC2":
					System.out.println("MC2 호출");                     //파일별 히스토리
					new ModifyComment2(socket, dis, dos);
					break;

				case "EX":													//종료
					System.out.println("EX 호출");
					socket.close();
					dis.close();
					break;


				default :
					System.out.println("메세지가 제대로 안왔어요 : "+message);
					dos.writeUTF("fail");
					break;
				}



			}
		} catch (IOException e) {
			System.out.println("클라이언트와 연결이 끊어졌습니다.");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

class ModifyComment1{			//파일 정보
	
	ModifyComment1(Socket socket, DataInputStream dis, DataOutputStream dos){
		try {
			dos.writeUTF("continue");
			
			String[] modify_data = dis.readUTF().split("&");			//file_id & file_comment
			DB.fileComment(modify_data[0], modify_data[1]);			
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}

class ModifyComment2{
	
	ModifyComment2(Socket socket, DataInputStream dis, DataOutputStream dos){
		
		try {

			
			String modify_data = dis.readUTF();				//modify_time, modify_user, file_id, comment;
			if(!modify_data.equals("cancel")){
				
				String[] tmp = modify_data.split("&");
				for(int i=0;i<tmp.length;i++){
					System.out.print(tmp[i] + " ");
				}
				
				String file_id = tmp[0];
				String modify_time = tmp[1];
				String modify_userid = tmp[2];
				String comment = tmp[3];
				
				DB.fileHistoryComment(file_id, modify_time, modify_userid, comment);
				
			}

			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}

class Check{
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	UserProcess up;
	String message;
	String src;

	SimpleDateFormat stringToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat dateToString = new SimpleDateFormat("yyyyMMddHHmmss");

	Check(Socket socket, DataInputStream dis, DataOutputStream dos, UserProcess up, String[] messageList){
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		this.up = up;


		try {

			//dos.writeUTF("continue");

			while(true){
				String file_id;
				message = dis.readUTF();


				for(int i=0;i< messageList.length; i++){
					if(message.equals(messageList[i])) return;
				}
				if(message.equals("end")) break;
				if(!message.equals("cancel")){
					//System.out.println("checkmessage : "+message);
					String info[] = message.split("&");					//project_id & file_name & modify_time & size & extentiON & comment 전달받음.

					file_id = DB.existFile(info[0],info[1]);
					if(file_id.equals("")){						//파일이 존재하지 않으면

						DB.insertFile(info[0], info[1], info[2], up.getID(), info[3], info[4], info[5]);	
						file_id = DB.existFile(info[0],info[1]);
						DB.insertFileHistory(info[0],file_id, info[1], info[2], up.getID(), info[5], info[3]);
						dos.writeUTF("ok");
						new ReceiveData(socket, dis, dos);
					}
					else{													//파일이 존재하면
						System.out.println("file is exist.");
						if(!DB.existFileHistory(info[0],info[1],info[2],info[3])){				//파일히스토리에 같은것이 존재하지 않으면
							DB.insertFileHistory(info[0],file_id, info[1], info[2], up.getID(), info[5], info[3]);
							dos.writeUTF("ok");														//보내달라고 요청한다.
							new ReceiveData(socket, dis, dos);											//파일을 받는다.


						}
						else{
							dos.writeUTF("no");
						}
					}

				}



			}
			while(true){

				int server_file_count = DB.Filecount(up.getID());
				String message = dis.readUTF();
				String sendMessage = "";
				String file_info = "";
				
				if(!message.equals("pass")){


					System.out.println("server file count : "+server_file_count);

					String server_tmp = DB.SearchFileInProject(up.getID());
					String[] tmp1 = null ;
					String[][] serverFile;
					String[][] result;


					String msg = dis.readUTF();
					System.out.println("msg what?  : "+msg);
					System.out.println("server_tmp what?  : "+server_tmp);


					if(!server_tmp.equals("")){
						tmp1 = server_tmp.split("&&");
						serverFile  = new String[tmp1.length][6];
						
						
						for(int i=0; i<serverFile.length;i++){
							serverFile[i] = tmp1[i].split("&");;
						}

						if(!msg.equals("none")){
							tmp1 = msg.split("&&");

							String[][] clientFile = new String[tmp1.length][3];



							for(int i=0; i<clientFile.length;i++){

								clientFile[i] = tmp1[i].split("&");
							}

							for(int i=0; i<clientFile.length;i++){
								System.out.println(i +" : "+clientFile[i][0]+" , "+clientFile[i][1]+ " , " + clientFile[i][2]);
							}



							result = CompareFileList(serverFile,clientFile);
						}
						else{

							result = serverFile;
						}
						for(int i=0;i<result.length;i++){
							System.out.println("result values : "+result[i][0] + " , " + result[i][1] + " , " + result[i][2]);
							if(!result[i][0].equals("")){
								sendMessage += result[i][0]+"&"+result[i][1]+"&"+result[i][2]+"&"+result[i][3]+"&"+result[i][4]+"&&";
								file_info += result[i][0]+"&"+result[i][1]+"&"+result[i][2]+"&"+result[i][3]+"&"+result[i][4]+"&"+result[i][5]+"&&";
							}
						}
						
						if(!sendMessage.equals("")){
							dos.writeUTF(sendMessage);
							System.out.println("request!!!!!!!! 파일보낼겁니다 !" + sendMessage);
							
							
							while(true){
								
								int i=0;
								String[] tmp = file_info.split("&&");
								
								new FileSenderForEach(socket, dos, dis, tmp[0]);
								
								if(dis.readUTF().equals("end")){
									break;
								}
								else{
									i++;
								}
							
							}
						}
						else{
							dos.writeUTF("equals");
						}


						break;
					}
					else{
						dos.writeUTF("equals");
						break;
					}

				}
				else{
					
					break;
				}
			}

		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}


	public String[][] CompareFileList(String[][] server, String[][] client) throws ParseException{


		SimpleDateFormat stringToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		for(int i=0;i<server.length;i++){
			System.out.println("server start : "+server[i][0] + " , " + server[i][1] + " , " + server[i][2]);



			server[i][2] = stringToDate.format(stringToDate.parse(server[i][2]));

			for(int j=0; j<client.length; j++){
				if(server[i][0].equals(client[j][0]) && server[i][1].equals(client[j][1]) && server[i][2].equals(client[j][2])){
					server[i][0] = "";
					server[i][1] = "";
					server[i][2] = "";
					server[i][3] = "";
					break;
				}
			}

		}





		return server;
	}

}

class CreateProject{

	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	String src;
	CreateProject(Socket socket, DataInputStream dis, DataOutputStream dos, UserProcess up){
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;

		System.out.println("프로젝트 개설 클래스 호출");

		try {
			String message = dis.readUTF();

			if(!message.equals("cancel")){

				System.out.println("프로젝트 개설 정보 : "+message);		//프로젝트 이름, 관리자 id, 시작일, 종료일, comment 정보 전송받음
				String[] cpInfo = message.split("&");
				Project newRegister = new Project(cpInfo);
				if(DB.insertProject(newRegister)){
					dos.writeBoolean(true);

					String project_id = DB.searchProjectId(cpInfo[0], cpInfo[1]);
					dos.writeUTF(project_id);

					src = System.getProperty("user.dir");
					File dir = new File(src+"/"+project_id);
					if(!dir.exists()){
						dir.mkdir();
					}

					//File[] fileList = dir.listFiles(); 
					dos.writeUTF(DB.searchProject(up.getID()));				//개설한 프로젝트 목록을 전송
					dos.writeUTF(DB.searchJoinProject(up.getID()));	 		//참여중인 프로젝트 목록을 전송



				}
				else{
					dos.writeBoolean(false);
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

class projectReload{

	projectReload(Socket socket, DataInputStream dis, DataOutputStream dos, UserProcess up){
		try {

			dos.writeUTF(DB.searchProject(up.getID()));
			dos.writeUTF(DB.searchJoinProject(up.getID()));	 		//참여중인 프로젝트 목록을 전송


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				//개설한 프로젝트 목록을 전송
	}

}

class JoinProject{

	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	UserProcess up;
	boolean pass = true;
	boolean search = false;
	JoinProject(Socket socket, DataInputStream dis, DataOutputStream dos, UserProcess up){
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		this.up = up;


		System.out.println("프로젝트 참여 클래스 호출");

		try {
			while(pass){

				String message = dis.readUTF();
				if(message.equals("ok")){
					dos.writeUTF("ok");
					message = dis.readUTF();
					if(DB.JoinProject(message,up.getID())){	//데이터 베이스에 입력 후 성공, 실패 결과 리턴
						dos.writeBoolean(true);
						break;
					}
					else{
						dos.writeBoolean(false);
					}

				}
				else if(message.equals("cancel")){
					break;				//취소를 클릭하면 while문 종료
				}
				else{
					if(message.equals(up.getID())){
						dos.writeUTF("null1");
					}
					else{
						System.out.println("검색할 관리자 id : "+message);	
						String tmp = DB.searchProject(message);				//&를 구분문자로 하여 프로젝트 이름들이 나열 



						System.out.println("tmp 값 : "+tmp);
						if(tmp.equals("null2")){
							dos.writeUTF("null2");
						}
						else{
							dos.writeUTF(tmp);		//해당 관리자가 개설한 프로젝트 목록 전송

							//message = dis.readUTF();		//개설자 id와 프로젝트 이름을 받아옴.

						}


					}


				}



				//DB.insertProject(newRegister);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}
}

class JoinProcess{
	Socket socket;
	DataInputStream dis;
	JoinProcess(Socket socket, DataInputStream dis,  DataOutputStream dos){
		this.socket = socket;
		this.dis = dis;
		System.out.println("회원가입 클래스 호출");
		try {
			while(true){
				String message = dis.readUTF();
				String[] idcheck = message.split("&");   


				if(message.equals("cancel")) {
					System.out.println("회원 가입 취소");
					break;
				}

				else if(idcheck[0].equals("idcheck")){

					System.out.println("정보 : " + message);

					boolean overlap = DB.IdCheck(idcheck[1]);

					if(overlap == true){
						System.out.println("중복");   
						dos.writeBoolean(true);
					}

					else{
						System.out.println("사용가능");
						dos.writeBoolean(false);
					}

				}

				else{
					System.out.println("회원 가입 정보 : "+message);
					String[] joinInfo = message.split("&");         //id, password, name, phone, email, department
					User newRegister = new User(joinInfo);
					DB.insertMember(newRegister);
					dos.writeUTF("success");
					break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

class getProject{

	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	UserProcess up;
	boolean pass = true;
	boolean search = false;
	getProject(Socket socket, DataInputStream dis, DataOutputStream dos, UserProcess up){
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		this.up = up;


		System.out.println("프로젝트 정보 송신 클래스 호출");

		try {
			dos.writeUTF("continue");
			String message = dis.readUTF();				//조회할 프로젝트 id를 받아옴.

			String tmp = DB.getProjectInfo(message);				//&를 구분문자로 하여 프로젝트 이름들이 나열 
			String file_list = DB.getFileListByProjectId(message);


			System.out.println("tmp 값 : "+tmp);
			if(tmp.equals("null2")){
				dos.writeUTF("null2");
			}
			else{
				dos.writeUTF(tmp);		// 프로젝트 정보 전송
				System.out.println("1");

			}

			dis.readUTF(); 					//continue 메세지 받으면 이어서 시작



			if(tmp.equals("null3")){
				dos.writeUTF("null3");
			}
			else{
				dos.writeUTF(file_list);		// 프로젝트 정보 전송
				System.out.println("2");

			}
			System.out.println(tmp);



			//DB.insertProject(newRegister);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}
}

class getApprove{

	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	UserProcess up;
	boolean pass = true;
	boolean search = false;
	String projectid;
	getApprove(Socket socket, DataInputStream dis, DataOutputStream dos, UserProcess up){
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		this.up = up;


		System.out.println("프로젝트 승인목록 클래스 호출");

		try {

			projectid = dis.readUTF();				//조회할 프로젝트 id를 받아옴.

			if(DB.isPM(projectid,up.getID())){					//만약 관리자가 맞으면?
				String tmp = DB.getProjectApprove(projectid);				//&를 구분문자로 하여 프로젝트 이름들이 나열 

				if(tmp.equals("null2")){
					dos.writeUTF("null2");
					process();
				}
				else{
					dos.writeUTF(tmp);		// 멤버 정보 전송
					process();
				}
			}
			else{
				dos.writeUTF("false");
			}





			//DB.insertProject(newRegister);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




	}
	public void process() throws IOException{

		String message;
		while(true){

			message = dis.readUTF();

			if(message.equals("cancel")) {
				System.out.println("관리 취소");
				break;
			}

			else if(message.equals("kickout")){
				System.out.println("수정 정보 : "+message);

				message = dis.readUTF();				//수정될 사용자 목록 넘겨받음
				String tmp[] = message.split("&");

				System.out.println("kickout : " + message);

				DB.MemberKickOut(tmp[0],tmp[1]);

				String result = DB.getProjectApprove(projectid);
				dos.writeUTF(result);
			}
			else if(message.equals("ModifyNotice")){
				//dos.writeUTF("continue");
				String notice = dis.readUTF();
				if(notice.equals("cancel")){
					break;
				}
				else{
					String modify[] = notice.split("&");

					DB.NoticeModify(modify[0], modify[1]);
					dos.writeUTF("sucess");
					break;

				}
			}
			else{

				message = dis.readUTF();				//수정될 사용자 목록 넘겨받음
				String tmp[] = message.split("&");

				System.out.println("approve : " + message);

				DB.MemberApprove(tmp[0],tmp[1]);


				String result = DB.getProjectApprove(projectid);
				dos.writeUTF(result);

			}
		}
	}
}


class LoginProcess{
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	UserProcess up;						//접속중인 아이디를 표시하기위해 넘겨받는다.

	LoginProcess(Socket socket, DataInputStream dis, DataOutputStream dos, UserProcess up){
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		this.up = up;

		System.out.println("로그인 클래스 호출");
		try {
			String message = dis.readUTF();
			if(message.equals("cancel") ) System.out.println("로그인 취소");
			else{
				System.out.println("로그인  정보 : "+message);

				String[] loginInfo = message.split("&");         //id, password

				boolean result = DB.Login(loginInfo[0], loginInfo[1]);

				if(result == true){
					System.out.println("로그인 성공");
					up.setID(loginInfo[0]);

					System.out.println(""+result);
					dos.writeBoolean(result);

					dos.writeUTF(DB.searchProject(up.getID()));				//개설한 프로젝트 목록을 전송
					dos.writeUTF(DB.searchJoinProject(up.getID()));	 		//참여중인 프로젝트 목록을 전송


				}
				else{

					System.out.println("로그인 실패");
					System.out.println(""+result);
					dos.writeBoolean(result);
				}



			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


class FileSender {				//파일 전송 클래스
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	FileInputStream fis;
	BufferedInputStream bis;
	BufferedReader reader;


	public FileSender(Socket socket, DataInputStream dis,  DataOutputStream dos, UserProcess up) throws ParseException {
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		String path = null;
		//String project_id = null;
		String modify_time = null;
		reader  = new BufferedReader(new InputStreamReader(System.in));
		String s = System.getProperty("user.dir");
		File f ;

		try {
			System.out.println("파일 전송 작업을 시작합니다.");

			// 파일 이름 전송

			SimpleDateFormat stringToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat dateToString = new SimpleDateFormat("yyyyMMddHHmmss");


			String project_array[] = DB.searchProjectIdByUserId(up.getID()).split("&");
			ArrayList<String> project_id = new ArrayList<String>();
			ArrayList<String> file_name = new ArrayList<String>();
			ArrayList<String> file_modify_time = new ArrayList<String>();
			ArrayList<String> file_modify_user = new ArrayList<String>();
			ArrayList<String> file_extension = new ArrayList<String>();
			String fileInfo = null;
			for(int i=0;i<project_array.length;i++){
				fileInfo = DB.searchFileIdByProjectId(project_array[i]);
				if(!fileInfo.equals("")){
					String linecut[] = fileInfo.split("&&");
					for(int j=0; j<linecut.length; j++){
						String tmp[] = linecut[j].split("&");
						project_id.add(project_array[i]);
						file_name.add(tmp[0]);
						file_modify_time.add(dateToString.format(stringToDate.parse(tmp[1])));
						file_modify_user.add(tmp[2]);
						file_extension.add(tmp[3]);
					}

					//System.out.println(fileInfo);
				}
			}
			int cnt = 0;
			if(project_id.size() > 0){
				for(int i=0;i<project_id.size();i++){
					System.out.println("project_id : "+project_id.get(i) + "  " +file_name.get(i) + "[T]" + file_modify_time.get(i) +"[N]"+file_modify_user.get(i)+"." +file_extension.get(i));
					f = new File(s+"/"+project_id.get(i)+"/"+file_name.get(i)+"[T]"+file_modify_time.get(i)+"[N]"+file_modify_user.get(i)+"."+file_extension.get(i));
					if(f.exists()){
						cnt++;
						String projectId = project_id.get(i);

						dos.writeUTF(projectId+"&"+file_name.get(i)+"."+file_extension.get(i)+"&"+file_modify_time.get(i));
						if(dis.readUTF().equals("request")){
							System.out.println("request");
							fis = new FileInputStream(f);
							bis = new BufferedInputStream(fis);
							dos.writeInt((int)f.length());



							int len;
							int size = 4096;
							byte[] data = new byte[size];
							while ((len = bis.read(data)) != -1) {
								dos.write(data, 0, len);
								System.out.print("#");
							}

							dis.readUTF();


							System.out.println("파일 전송 작업을 완료하였습니다.");
							System.out.println("보낸 파일의 사이즈 : " + f.length());
						}
						else{
							System.out.println("exists");
							cnt--;
						}
					}
					else{
						System.err.println("file path : "+f.getPath());
						System.err.println(file_name.get(i)+"."+file_extension.get(i)+" 파일을 찾을 수 없습니다.");
					}


				}
				if(cnt > 0){
					dos.flush();
					bis.close();
					fis.close();
				}
			}
			dos.writeUTF("end");

			//System.out.printf("파일 이름(%s)을 전송하였습니다.\n", f.getName());


			//System.out.println("현재 디렉토리는 " + s + " 입니다");



			// 파일 내용을 읽으면서 전송

			//DateFormat df = new SimpleDateFormat("HH:mm:ss"); // HH=24h, hh=12h
			//String str = df.format(f.lastModified());
			//Date df = new Date(f.lastModified());



			//dos.close();

			//reader.close();


		} catch (IOException e) {
			e.printStackTrace();
		}


	}
}


class FileHistory_Sender{				//파일히스토리 전송
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	FileInputStream fis;
	BufferedInputStream bis;
	BufferedReader reader;


	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat file_df = new SimpleDateFormat("yyyyMMddHHmmss");				//파일이름 뒤에 추가할 날짜시간


	public FileHistory_Sender(Socket socket, DataOutputStream dos, DataInputStream dis) throws IOException, ParseException {
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		reader  = new BufferedReader(new InputStreamReader(System.in));
		File f;
		//dos.writeUTF("continue");

		String s = System.getProperty("user.dir");
		String str = dis.readUTF();							//project_id & file_name & modify_time & modify_userid

		try {
			System.out.println("start file send process.");

			String message[] = str.split("&");			
			// 파일 이름 전송


			String project_id = message[0];
			String file_name = message[1];
			Date n = df.parse(message[2]);
			String modify_time = file_df.format(n);
			String modify_user = message[3];




			System.out.println(" 요청 메시지 : " +project_id+"&"+file_name+"&"+modify_time);
			//System.out.print("프로젝트 이름 입력 ");

			String tmp[] = file_name.split("[.]");

			f = new File(s+"/"+project_id+"/"+tmp[0]+"[T]"+modify_time+"[N]"+modify_user+"."+tmp[1]);
			if(f.exists()){

				System.out.println("request");
				fis = new FileInputStream(f);
				bis = new BufferedInputStream(fis);
				dos.writeInt((int)f.length());

				int len;
				int size = 4096;
				byte[] data = new byte[size];
				while ((len = bis.read(data)) != -1) {
					dos.write(data, 0, len);
					System.out.print("#");
				}

				dis.readUTF();				//확인메세지 받음


				System.out.println("파일 전송 작업을 완료하였습니다.");
				System.out.println("보낸 파일의 사이즈 : " + f.length());

			}
			else{
				System.err.println("file path : "+ f.getPath());
				System.err.println(tmp[0]+"."+tmp[1]+" 파일을 찾을 수 없습니다.");
			}


			//dos.close();
			dos.flush();
			bis.close();
			fis.close();

			//reader.close();


		} catch (IOException e) {
			e.printStackTrace();
		}


	}
}

class FileSenderForEach{				//각각의 파일 전송 클래스
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	FileInputStream fis;
	BufferedInputStream bis;
	BufferedReader reader;


	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat file_df = new SimpleDateFormat("yyyyMMddHHmmss");				//파일이름 뒤에 추가할 날짜시간


	public FileSenderForEach(Socket socket, DataOutputStream dos, DataInputStream dis, String file_info) throws IOException, ParseException {
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		reader  = new BufferedReader(new InputStreamReader(System.in));
		File f;
		//dos.writeUTF("continue");

		String s = System.getProperty("user.dir");

		try {
			System.out.println("start file send process.");

			String message[] = file_info.split("&");
		

			String project_id = message[0];
			String file_name = message[1];
			Date n = df.parse(message[2]);
			String modify_time = file_df.format(n);
			String extention = message[4];
			String modify_user = message[5];




			System.out.println(" 요청 메시지 : " +project_id+"&"+file_name+"&"+modify_time);
			//System.out.print("프로젝트 이름 입력 ");


			f = new File(s+"/"+project_id+"/"+file_name+"[T]"+modify_time+"[N]"+modify_user+"."+extention);
			if(f.exists()){

				System.out.println("request");
				fis = new FileInputStream(f);
				bis = new BufferedInputStream(fis);
				dos.writeInt((int)f.length());

				int len;
				int size = 4096;
				byte[] data = new byte[size];
				while ((len = bis.read(data)) != -1) {
					dos.write(data, 0, len);
					System.out.print("#");
				}



				System.out.println("파일 전송 작업을 완료하였습니다.");
				System.out.println("보낸 파일의 사이즈 : " + f.length());

			}
			else{
				System.err.println("file path : "+ f.getPath());
				System.err.println(file_name+"."+extention+" 파일을 찾을 수 없습니다.");
			}


			//dos.close();
			dos.flush();
			bis.close();
			fis.close();

			//reader.close();


		} catch (IOException e) {
			e.printStackTrace();
		}


	}
}


class ReceiveData{
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	FileOutputStream fos;
	BufferedOutputStream bos;
	String src;
	ReceiveData(Socket socket, DataInputStream dis, DataOutputStream dos){
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		try{
			// 파일명을 전송 받고 파일명 수정.
			String str = dis.readUTF();					// user ID, fileName을 받는다.
			String split[] = str.split("&");			//&기호를 기준으로 id와 파일명으로 나눈다. 
			String project_id = split[0];					//추후에 프로젝트id로 수정해야 함. (서버 내에 프로젝트 기준으로 정렬)
			String fName = split[1];					//파일 이름으로 된 폴더 내에 정렬
			src = System.getProperty("user.dir");

			System.out.println("파일명 " + fName + "을 전송받았습니다.");
			//fName = fName.replaceAll("a", "b");

			// 파일을 생성하고 파일에 대한 출력 스트림 생성

			File desti = new File(src+"/"+project_id);
			if(!desti.exists()){
				//없다면 생성
				desti.mkdirs(); 
			}else{

			}
			System.out.println("파일명은 : "+fName);
			File f = new File(src+"/"+project_id+"/"+fName);
			fos = new FileOutputStream(f);
			bos = new BufferedOutputStream(fos);
			System.out.println("접속 ID : " + project_id);
			System.out.println(fName + "파일을 생성하였습니다.");

			int fSize = dis.readInt();

			// 바이트 데이터를 전송받으면서 기록
			int len;
			int size = 4096;
			byte[] data = new byte[size];
			while ((len = dis.read(data)) != -1) {
				//if(len < size) break;
				bos.write(data, 0, len);
				//	System.out.println(len);
				fSize -= len;
				if(fSize <= 0) break;

			}

			dos.writeUTF("end");
			//bos.write(data, 0, len);

			bos.flush();
			bos.close();
			fos.close();
			//dis.close();
			System.out.println("파일 수신 작업을 완료하였습니다.");
			System.out.println("받은 파일의 사이즈 : " + f.length());
		}catch (IOException e){


		}
	}
}

class MemberModify{
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	MemberModify(Socket socket, DataInputStream dis,  DataOutputStream dos){
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		System.out.println("회원수정 클래스 호출");
		try {
			while(true){
				String message = dis.readUTF();
				String[] check = message.split("&");   

				for(int i = 0; i<check.length; i++){
					System.out.println("나오닝"+check[i]);
				}

				if(message.equals("cancel")) {
					System.out.println("회원수정 취소");
					break;
				}

				else if( check[0].equals("check")){
					System.out.println("비밀번호 변경");
					DB.PwdModify(check[1], check[2]);
					break;
				}

				else{
					System.out.println("수정 정보 : "+message);
					String[] modifyInfo = message.split("&");         //name, phone, email, department
					User newRegister = new User(modifyInfo);
					DB.MemberModify(newRegister);
					break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
class FileHistory{
	Socket socket;
	DataInputStream dis;
	DataOutputStream dos;
	FileHistory(Socket socket, DataInputStream dis,  DataOutputStream dos){
		this.socket = socket;
		this.dis = dis;
		this.dos = dos;
		System.out.println("파일 히스토리 클래스 호출");
		try {
			String message = dis.readUTF();				//file_id 받아옴
			System.out.println("뭐가 온거니"+message);

			String result = DB.FileHistory(message);				//히스토리목록
			String file_info = DB.FileHistoryInfo(message);			//파일정보

			if(result.equals(null)){
				dos.writeUTF(null);
			}else{
				dos.writeUTF(result);
			}

			if(file_info.equals(null)){
				dos.writeUTF(null);
			}else{
				dos.writeUTF(file_info);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}