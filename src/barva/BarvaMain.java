package barva;


/*===========================================
 
 Barva 프로젝트 서버 메인.
 콘솔 형식으로 작업 계획.
 
 ============================================*/
	

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class BarvaMain {
	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {

		//new UserRegister();
		
		ServerSocket serverSocket = null;
		Socket socket = null;
		try {
			serverSocket = new ServerSocket(9999);
			// 리스너 소켓 생성 후 대기
			System.out.println("서버가 시작되었습니다.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		while(true){
			try {
				// 연결되면 통신용 소켓 생성
				socket = serverSocket.accept();
				System.out.println("클라이언트와 연결되었습니다.");

				// 파일 수신 작업 시작
				UserProcess fr = new UserProcess(socket);			//각 소켓 번호를 통해 각 사용자의 작업을 처리한다.
				fr.start();											//작업시작
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
}