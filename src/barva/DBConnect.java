package barva;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.omg.CORBA.Request;

public class DBConnect {

	// TODO Auto-generated method stub
	static Connection con = null;
	static Statement stmt = null;
	static ResultSet rs = null;
	static PreparedStatement prStmt = null;

	public static boolean loadConnect() {
		try {
			// 드라이버 로딩
			Class.forName("com.mysql.jdbc.Driver");

		} catch (java.lang.ClassNotFoundException e) {
			
			
			System.err.println("** Driver loaderror in loadConnect: " + e.getMessage());
			e.printStackTrace();
		}

		try {
			// 연결하기 - HSbank 데이터베이스와 연결
			String URL = "jdbc:mysql://localhost:3306/barva";
			con = DriverManager.getConnection(URL, "root", "admin!1234");
			System.out.println("\n  연결 성공 바르바: " + URL + "에 연결됨");

			return true;
		} catch (SQLException ex) {
			System.err.println("** connection error in loadConnect: " + ex.getMessage());
			ex.printStackTrace();
		}

		return false;
	}

}
