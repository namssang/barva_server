package barva;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DB extends DBConnect{

	static boolean insertMember(User re)  {       
		//boolean updateCnt = false;      
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "insert into user values (?,?,?,?,?,?);" ;
			prStmt = con.prepareStatement(sql);


			prStmt.setString(1, re.userId); 
			prStmt.setString(2, re.password);
			prStmt.setString(3, re.name);
			prStmt.setString(4, re.phone);
			prStmt.setString(5, re.email);
			prStmt.setString(6, re.department);

			prStmt.executeUpdate();        
		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertuser: " + ex.getMessage() );
			return false;
		}
		System.out.println("\n회원가입이 완료되었습니다.\n");
		return true;
	}


	static boolean insertProject(Project pj)  {  	     
		int updateCnt = 0;

		try {                      
			// SQL 질의문을 수행한다.
			String sql = "insert into project (project_name,pm,finish_time,comment) values (?,?,?,?);" ;
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, pj.projectSubject);
			prStmt.setString(2, pj.pm);
			prStmt.setString(3, pj.finDt);
			prStmt.setString(4, pj.comment);

			updateCnt = prStmt.executeUpdate();        
		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
			return false;
		}

		return true;

	}

	static String searchProjectId(String project_name, String pm){
		int cnt = 0;
		String result = "";

		PreparedStatement pstmt = null ;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project where project_name = (?) and pm = (?)" ;
			pstmt = con.prepareStatement(sql);

			//System.out.println("검색할 아이디 : "+id);

			pstmt.setString(1, project_name);
			pstmt.setString(2, pm);




			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				/*
				try {
					result += new String(rs.getString("project_name").getBytes(), "MS949");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 */
				result += new String(rs.getString("project_id"));

			}

		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
		}
		return result;

	}


	static String searchProject(String id)  {  	     
		int cnt = 0;
		String result = "";

		PreparedStatement pstmt = null ;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project where pm = (?)" ;
			pstmt = con.prepareStatement(sql);

			System.out.println("검색할 아이디 : "+id);

			pstmt.setString(1, id);



			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				/*
				try {
					result += new String(rs.getString("project_name").getBytes(), "MS949");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 */
				result += new String(rs.getString("project_id"));
				result += "&";
				result += new String(rs.getString("project_name"));
				result += "&&";
				cnt ++;
			} 	         

			if (cnt == 0 ){
				result = "null2";

			}



		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
		}
		System.out.println("result : "+result);
		return result;
	}


	static String searchJoinProject(String id)  {  	     
		int cnt = 0;
		String result = "";

		PreparedStatement pstmt = null ;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select project_id, project_name from project where project_id "
					+ "IN (Select project_id from project_member where user_id = (?) and request = 1)" ;
			pstmt = con.prepareStatement(sql);

			System.out.println("검색할 아이디 : "+id);

			pstmt.setString(1, id);



			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				/*
				try {
					result += new String(rs.getString("project_name").getBytes(), "MS949");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 */
				result += new String(rs.getString("project_id"));
				result += "&";
				result += new String(rs.getString("project_name"));
				result += "&&";
				cnt ++;
			} 	         

			if (cnt == 0 ){
				result = "null2";

			}



		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
		}
		System.out.println("result : "+result);
		return result;
	}


	static String approveList(String id)  {  	     
		int cnt = 0;
		String result = "";

		PreparedStatement pstmt = null ;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project where pm = (?)" ;
			pstmt = con.prepareStatement(sql);

			System.out.println("검색할 아이디 : "+id);

			pstmt.setString(1, id);



			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				/*
				try {
					result += new String(rs.getString("project_name").getBytes(), "MS949");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 */
				result += new String(rs.getString("project_id"));
				result += "&";
				result += new String(rs.getString("project_name"));
				result += "&&";
				cnt ++;
			} 	         

			if (cnt == 0 ){
				result = "null2";

			}



		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
		}
		System.out.println("result : "+result);
		return result;
	}

	static boolean Login(String userId, String password){
		boolean cnt = false;
		PreparedStatement pstmt = null ;
		try{
			String sql = "select * from user where user_ID=? and password=?;" ;

			pstmt = con.prepareStatement(sql) ;

			pstmt.setString(1, userId);
			pstmt.setString(2, password);

			rs = pstmt.executeQuery();
			if (rs.next()) 
			{
				cnt = true;
			} 	         
			else 
				cnt = false;
		} catch (SQLException ex) {
			System.err.println("\n  ?? SQL문 결과출력 오류:" + ex.getMessage());
		}
		return cnt;
	}

	static boolean JoinProject(String number, String title){  
		PreparedStatement pstmt = null ;


		try{
			String sql = "insert into project_member (project_id, user_id, request ) values (?,?,?);" ;


			pstmt = con.prepareStatement(sql) ;

			pstmt.setString(1, number);
			pstmt.setString(2, title);
			pstmt.setInt(3, 0);


			pstmt.executeUpdate();  
		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
			return false;
		}
		return true;

	}

	static boolean IdCheck(String userId){
		boolean cnt = false;
		PreparedStatement pstmt = null ;
		try{
			String sql = "select * from user where user_ID=? ;";

			pstmt = con.prepareStatement(sql) ;

			pstmt.setString(1, userId);

			rs = pstmt.executeQuery();
			if (rs.next()) 
			{
				cnt = true;
			}             
			else 
				cnt = false;
		} catch (SQLException ex) {
			System.err.println("\n  ?? SQL문 결과출력 오류:" + ex.getMessage());
		}
		return cnt;
	}
	static String getProjectInfo(String id)  {  	     			//프로젝트 모든정보를 내보낸다.
		int cnt = 0;
		String result = "";

		PreparedStatement pstmt = null ;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project where project_id = (?)" ;
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, id);



			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				/*
				try {
					result += new String(rs.getString("project_name").getBytes(), "MS949");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 */
				result += new String(rs.getString("project_id"));
				result += "&";
				result += new String(rs.getString("project_name"));
				result += "&";
				result += new String(rs.getString("pm"));
				result += "&";
				result += new String(rs.getString("reg_time"));
				result += "&";
				result += new String(rs.getString("finish_time"));
				result += "&";
				result += new String(rs.getString("comment"));
				cnt ++;
			} 	         

			if (cnt == 0 ){
				result = "null2";

			}



		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
		}
		return result;
	}

	static String getFileListByProjectId(String project_id)  {  	     			//프로젝트 모든정보를 내보낸다.
		int cnt = 0;
		String result = "";

		PreparedStatement pstmt = null ;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project_file where project_id = (?)" ;
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, project_id);



			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				/*
				try {
					result += new String(rs.getString("project_name").getBytes(), "MS949");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 */
				result += new String(rs.getString("file_id"));
				result += "&";
				result += new String(rs.getString("file_name"));
				result += "&";
				result += new String(rs.getString("extension"));
				result += "&&";
				cnt ++;
			} 	         

			if (cnt == 0 ){
				result = "null3";

			}



		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
		}
		return result;
	}
	static String getProjectApprove(String id)  {  	     			//프로젝트 모든정보를 내보낸다.
		int cnt = 0;
		String result = "";

		PreparedStatement pstmt = null ;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project_member where project_id = (?)" ;
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, id);



			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				/*
				try {
					result += new String(rs.getString("project_name").getBytes(), "MS949");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 */
				result += new String(rs.getString("project_id"));
				result += "&";
				result += new String(rs.getString("user_id"));
				result += "&";
				result += new String(rs.getString("request"));
				result += "&&";
				cnt ++;
			} 	         

			if (cnt == 0 ){
				result = "null2";

			}



		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
		}
		return result;
	}

	static boolean isPM(String project_id, String user_id)  {  	     			//프로젝트 모든정보를 내보낸다.
		int cnt = 0;

		PreparedStatement pstmt = null ;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project where project_id = (?) and pm = (?);" ;
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, project_id);
			pstmt.setString(2, user_id);



			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				cnt ++;
			} 	         

			if (cnt == 0 ){
				return false;

			}



		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
		}
		return true;
	}

	static void MemberModify(User re)  {       
		//boolean updateCnt = false;      
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "update user set name =?, phone = ?, email = ?, department = ? where user_id = ?" ;
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, re.name);
			prStmt.setString(2, re.phone);
			prStmt.setString(3, re.email);
			prStmt.setString(4, re.department);
			prStmt.setString(5, re.userId);

			prStmt.executeUpdate();        
		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertuser: " + ex.getMessage() );
		}
		System.out.println("\n회원수정이 완료되었습니다.\n");
	}

	static void MemberKickOut(String project_id, String user_id){

		try{
			String sql = "delete from project_member where project_id = (?) and user_id = (?)";
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, project_id);
			prStmt.setString(2, user_id);

			prStmt.executeUpdate();     

		}catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertuser: " + ex.getMessage() );
		}

	}
	static void MemberApprove(String project_id, String user_id){

		try{
			String sql = "update project_member set request = 1 where project_id = (?) and user_id = (?)";
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, project_id);
			prStmt.setString(2, user_id);

			prStmt.executeUpdate();     

		}catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertuser: " + ex.getMessage() );
		}

	}

	static void PwdModify(String id, String pwd)  {       
		//boolean updateCnt = false;      
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "update user set password =? where user_id = ?" ;
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, pwd);
			prStmt.setString(2, id);

			prStmt.executeUpdate();        
		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in PwdModify: " + ex.getMessage() );
		}
		System.out.println("\n회원수정이 완료되었습니다.\n");
	}

	static void fileComment(String id, String data)  {       
		//boolean updateCnt = false;      
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "update project_file set comment =? where file_id = ?" ;
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, data);
			prStmt.setString(2, id);

			prStmt.executeUpdate();        
		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in fileComment: " + ex.getMessage() );
		}
		System.out.println("\n파일 정보 변경 완료되었습니다.\n");
	}
	
	static void fileHistoryComment(String file_id, String modify_time, String modify_userid, String data)  {       
		//boolean updateCnt = false;      
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "update file_list set comment =? where modify_time = ? and modify_user_id = ? and file_id = ?" ;
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, data);
			prStmt.setString(2, modify_time);
			prStmt.setString(3, modify_userid);
			prStmt.setString(4, file_id);

			prStmt.executeUpdate();        
		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in FileHistoryComment: " + ex.getMessage() );
		}
		System.out.println("\n파일 히스토리 정보 변경 완료되었습니다.\n");
	}

	
	static String FileHistory(String file_id)  {       
		String result = "";

		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from file_list where file_id = ? order by modify_time asc" ;
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, file_id);

			rs = prStmt.executeQuery();      

			while (rs.next()) 
			{
				result += new String(rs.getString("modify_time"));
				result += "&";
				result += new String(rs.getString("modify_user_id"));
				result += "&";
				result += new String(rs.getString("comment"));
				result += "&";
			}             

		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in FileHistory: " + ex.getMessage() );
		}

		System.out.println("\n히스토리 불러와졌을깡.\n");
		return result;
	}

	static String existFile(String project_id, String file_name)  {  	     			//프로젝트 모든정보를 내보낸다.
		int cnt = 0;
		String result = null;

		PreparedStatement pstmt = null ;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project_file where project_id = (?) and file_name = (?);" ;
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, project_id);
			pstmt.setString(2, file_name);



			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				result = rs.getString("file_id");
				cnt ++;
			} 	         

			if (cnt == 0 ){
				return "";

			}



		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
		}
		return result;
	}

	static boolean insertFile(String project_id, String file_name, String modify_time, String modify_user, String size, String extention, String comment)  {  	     
		int updateCnt = 0;

		try {                      
			// SQL 질의문을 수행한다.
			String sql = "insert into project_file (project_id,file_name,modify_time,modify_userid,size,extension,comment) values (?,?,?,?,?,?,?);" ;
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, project_id);
			prStmt.setString(2, file_name);
			prStmt.setString(3, modify_time);
			prStmt.setString(4, modify_user);
			prStmt.setString(5, size);
			prStmt.setString(6, extention);
			prStmt.setString(7, comment);

			updateCnt = prStmt.executeUpdate();        
		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertProject: " + ex.getMessage() );
			return false;
		}

		return true;
	}
	static boolean existFileHistory(String project_id, String file_name, String modify_time, String size)  {  	     			//프로젝트 모든정보를 내보낸다.
		int cnt = 0;

		PreparedStatement pstmt = null ;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from file_list where project_id = (?) and file_name = (?) and modify_time = (?) and size = (?);" ;
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, project_id);
			pstmt.setString(2, file_name);
			pstmt.setString(3, modify_time);
			pstmt.setString(4, size);



			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				cnt ++;
			} 	         

			if (cnt == 0 ){
				return false;

			}



		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in existFileHistory: " + ex.getMessage() );
		}
		return true;
	}

	static boolean insertFileHistory(String project_id,String file_id, String file_name, String modify_time, String modify_user, String comment, String size) throws ParseException  {  	     
		int updateCnt = 0;
		PreparedStatement pstmt = null ;
		String result = "";
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "insert into file_list (project_id,file_id,file_name,modify_time,modify_user_id,comment,size) values (?,?,?,?,?,?,?);" ;
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, project_id);
			prStmt.setString(2, file_id);
			prStmt.setString(3, file_name);
			prStmt.setString(4, modify_time);
			prStmt.setString(5, modify_user);
			prStmt.setString(6, comment);
			prStmt.setString(7, size);

			updateCnt = prStmt.executeUpdate();

			sql = "select * from project_file where file_id = (?);" ;
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, file_id);
			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				result += rs.getString("modify_time");
			}
			if(dt.parse(result).compareTo(dt.parse(modify_time))==-1){
				sql = "update project_file set modify_time = (?), modify_userid = (?) where file_id = (?) ";
				prStmt = con.prepareStatement(sql);
				prStmt.setString(1, modify_time);
				prStmt.setString(2, modify_user);
				prStmt.setString(3, file_id);

				updateCnt = prStmt.executeUpdate();
			}

		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertFileHistory: " + ex.getMessage() );
			return false;
		}

		return true;
	}

	static String searchLatelyFile(String file_id){
		int cnt = 0;
		PreparedStatement pstmt = null ;
		String result=null;
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from file_list where file_id = (?) order by modify_time desc limit 1;" ;
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, file_id);



			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				result = rs.getString("modify_time");
				cnt ++;
			} 	         

			if (cnt == 0 ){
				return "";

			}



		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in existFileHistory: " + ex.getMessage() );
		}
		return result;
	}

	static String searchProjectIdByUserId(String user_id){
		int cnt = 0;
		PreparedStatement pstmt = null ;
		String result="";
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project where pm = (?)" ;
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, user_id);

			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				result += rs.getString("project_id");
				result += "&";
			} 	         

			sql = "select * from project_member where user_id = (?) and request = 1";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, user_id);

			rs = pstmt.executeQuery();  
			while (rs.next()) 
			{
				result += rs.getString("project_id");
				result += "&";
			} 	         

		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in existFileHistory: " + ex.getMessage() );
		}
		return result;
	}

	static String searchFileIdByProjectId(String project_id){
		int cnt = 0;
		PreparedStatement pstmt = null ;
		String result="";
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project_file where project_id = (?)" ;
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, project_id);

			rs = pstmt.executeQuery();   
			while (rs.next()) 
			{
				result += rs.getString("file_name");
				result += "&";
				result += rs.getString("modify_time");
				result += "&";
				result += rs.getString("modify_userid");
				result += "&";
				result += rs.getString("extension");
				result += "&&";
				cnt ++;
			} 	         
			if(cnt == 0){
				return "";
			}

		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in existFileHistory: " + ex.getMessage() );
		}
		return result;
	}


	static String FileHistoryInfo(String file_id)  {       
		String result = "";

		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select * from project_file where file_id = ?" ;
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, file_id);

			rs = prStmt.executeQuery();      

			while (rs.next()) 
			{
				result += new String(rs.getString("file_name"));
				result += "&";
				result += new String(rs.getString("comment"));
				result += "&";
			}             

		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in FileHistory: " + ex.getMessage() );
		}

		System.out.println("\n히스토리 불러와졌을깡.\n");
		return result;
	}


	static void NoticeModify(String project_id, String notice)  {       
		//boolean updateCnt = false;      
		try {                      
			// SQL 질의문을 수행한다.
			String sql = "update project set comment =? where project_id = ?" ;
			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, notice);
			prStmt.setString(2, project_id);

			prStmt.executeUpdate();        
		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in insertuser: " + ex.getMessage() );
		}
		System.out.println("\n공지사항 수정이이 완료되었습니다.\n");
	}

	static int Filecount(String user_id){

		String result = "";

		try {                      
			// SQL 질의문을 수행한다.
			String sql = "select (count(file_id)+(select count(file_id) from project_file where project_file.PROJECT_ID in (select project_id from project where PM = ?))) as 'count' from project_file where project_file.PROJECT_ID in ((select project_id from project_member where user_id = ? and request = 1)) ;";

			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, user_id);

			prStmt.setString(2, user_id);

			rs = prStmt.executeQuery();      

			while (rs.next()) 
			{
				result += new String(rs.getString("count"));
			}             

		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in FileHistory: " + ex.getMessage() );
		}

		int count = Integer.parseInt(result);

		return count;
	}

	static String SearchFileInProject(String user_id){

		String result = "";

		try {                      
			// SQL 질의문을 수행한다.
			String sql = "  select project.project_id, project.project_name, project_file.file_name,"
					+ " project_file.MODIFY_TIME, project_file.EXTENSION,project_file.MODIFY_USERID from project,project_file where project.project_id ="
					+ " project_file.project_id and project_file.PROJECT_ID IN (select project_id from "
					+ "project_member where user_id = ? and request = 1) union  select project.project_id,"
					+ " project.project_name, project_file.file_name, project_file.MODIFY_TIME, project_file.EXTENSION,project_file.MODIFY_USERID from project,project_file "
					+ "where project.project_id = project_file.project_id and project_file.PROJECT_ID IN "
					+ "(select project_id from project where PM = ?);" ;

			prStmt = con.prepareStatement(sql);

			prStmt.setString(1, user_id);

			prStmt.setString(2, user_id);

			rs = prStmt.executeQuery();      

			while (rs.next()) 
			{
				result += new String(rs.getString("project_id"));
				result += "&";
				result += new String(rs.getString("file_name"));
				result += "&";
				result += new String(rs.getString("modify_time"));
				result += "&";
				result += new String(rs.getString("project_name"));
				result += "&";
				result += new String(rs.getString("extension"));
				result += "&";
				result += new String(rs.getString("modify_userid"));
				result += "&&";
				
			}             

		} catch( SQLException ex ) {

			System.err.println("** SQL exec error in FileHistory: " + ex.getMessage() );
		}


		return result;
	}

}
