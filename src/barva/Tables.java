package barva;

import java.util.Date;

/*===========================================
 
 모든 테이블 기본정보 관리를 이 파일에서 한다.
 
 ============================================*/
	



public class Tables  {


}


/*===========================================
 
 회원가입된 사용자 정보
 
 ============================================*/
	
class User{
	String userId;
	String password;
	String name;
	String phone;
	String email;
	String department;

	public User(String[] joinInfo) {
		this.userId = joinInfo[0];
		this.password = joinInfo[1];
		this.name = joinInfo[2];
		this.phone = joinInfo[3];
		this.email = joinInfo[4];
		this.department = joinInfo[5];		
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}


}


/*===========================================
 
 프로젝트의 정보 테이블
 
 ============================================*/
	

class Project {
	
	
	String projectSubject;
	String pm;
	String finDt;
	String comment;

	public Project( String projectSubject, String pmt,
			String finDt, String comment) {
		this.projectSubject = projectSubject;
		this.pm = pmt;
		this.finDt = finDt;
		this.comment = comment;
	}

	public Project( String[] cpInfo) {
		this.projectSubject = cpInfo[0];
		this.pm = cpInfo[1];
		this.finDt = cpInfo[2];
		this.comment = cpInfo[3];
	}
	public String getProjectSubject() {
		return projectSubject;
	}

	public void setProjectSubject(String projectSubject) {
		this.projectSubject = projectSubject;
	}

	public String getPm() {
		return pm;
	}

	public void setPm(String pm) {
		this.pm = pm;
	}

	public String getFinDt() {
		return finDt;
	}

	public void setFinDt(String finDt) {
		this.finDt = finDt;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}



/*===========================================
 
 각 프로젝트에 소속되어있는 팀원정보
 
 ============================================*/
	
class project_member{
	int project_id;
	String user_id;
	boolean request;
	

	public int getProject_id() {
		return project_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public boolean isRequest() {
		return request;
	}
	public void setRequest(boolean request) {
		this.request = request;
	}
}



/*===========================================
 
 각 프로젝트에 해당하는 파일 정보
 
 ============================================*/
	
class project_file{
	int project_id;
	int file_id;
	String file_name;
	String modify_time;
	String modify_user_id;
	int size;
	String extension;
	String comment;
	public int getProject_id() {
		return project_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public int getFile_id() {
		return file_id;
	}
	public void setFile_id(int file_id) {
		this.file_id = file_id;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getModify_time() {
		return modify_time;
	}
	public void setModify_time(String modify_time) {
		this.modify_time = modify_time;
	}
	public String getModify_user_id() {
		return modify_user_id;
	}
	public void setModify_user_id(String modify_user_id) {
		this.modify_user_id = modify_user_id;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}



/*===========================================
 
 파일들의 정보
 
 ============================================*/
	

class file_list{
	int project_id;
	int file_id;
	String file_name;
	String modify_time;
	String modify_user_id;
	String comment;
	public int getProject_id() {
		return project_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public int getFile_id() {
		return file_id;
	}
	public void setFile_id(int file_id) {
		this.file_id = file_id;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getModify_time() {
		return modify_time;
	}
	public void setModify_time(String modify_time) {
		this.modify_time = modify_time;
	}
	public String getModify_user_id() {
		return modify_user_id;
	}
	public void setModify_user_id(String modify_user_id) {
		this.modify_user_id = modify_user_id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}